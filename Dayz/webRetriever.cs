﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Windows.Forms;
using System.Threading;

namespace Dayz
{
    class webRetriever
    {
        public string url { get; set; }
        private List<string> files;
        private List<string> folders;
        //private List<string> betaLinks;

        public webRetriever(string httpUrl, bool beta = false)
        {
            url = httpUrl;

            files = new List<string>();
            folders = new List<string>();

            // Calling methods that gets the list of files in the url
            getFileList(beta);
        }

        public webRetriever()
        {
        }

        public string downloadText(string url_)
        {
            System.Text.Encoding enc = System.Text.Encoding.ASCII;

            WebClient wc = new WebClient();
            string changelog = "";
            try
            {
                changelog = enc.GetString(wc.DownloadData(url_));
            }
            catch (Exception e)
            {
                MessageBox.Show("ERROR: " + e, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                changelog = "";
            }

            return changelog;
        }

        public List<string> getFiles()
        {
            return files;
        }

        public void downloadFiles(string file2Download, string folder)
        {
            //WebClient wc = new WebClient();
            loading download = new loading();
            download.Show();
            download.downloadFile(url, file2Download, folder);
        }

        public void downloadFiles(List<string>files2Download, string folder)
        {
            WebClient wc = new WebClient();
            loading progress = new loading();
            progress.setMax(files2Download.Count);
            progress.refreshBar();
            progress.setLabel("Downloading...");
            progress.Show();
            for (int i = 0; i < files2Download.Count; ++i)
            {
                progress.addOne();
                progress.refreshBar();
                if (files2Download[i].Trim() != "")
                {
                    wc.DownloadFile(url + files2Download[i], folder + files2Download[i]);
                    progress.addOne();
                    progress.refreshBar();
                }
            }
            progress.Close();
        }

        private bool getFileList(bool beta = false)
        {
            // Downloading the text from the page and saving to this string
            string htmlCode = "";
            try
            {
                // Connecting
                WebClient wc = new WebClient();
                htmlCode = wc.DownloadString(url);
            }
            catch (Exception e)
            {
                MessageBox.Show("Could not connect to server: " + e.ToString(), "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            if (htmlCode == "")
            {
                MessageBox.Show("Page is empty", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            // If we are going through the beta links
            if (beta == true)
            {
                // Putting lines to a list
                //List<string> htmlLines = new List<string>();
                //htmlLines.Add(htmlCode);

                files = new List<string>(); // Class variable
                //string htmlLine = htmlLines[i];
                // Separating all links
                string[] linkTemp = new string[1];
                linkTemp[0] = "<a href=";
                List<string> betaHtml = new List<string>(htmlCode.Split(linkTemp, StringSplitOptions.None));

                // Now links are separated, but they are in format of "'ftp://downloads.bistudio.com/arma2.com/update/beta/ARMA2_OA_Build_94700.zip'>ARMA2_OA_Build_94700.zip</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Changelog: "
                // so that needs to be changed
                for (int j = 0; j < betaHtml.Count(); ++j)
                {
                    // Removing the ' from the beginning
                    betaHtml[j] = betaHtml[j].Remove(0, 1); 

                    // Checking that this line is one of the ftp-links
                    if (betaHtml[j].Substring(0, 6) != "ftp://" || betaHtml[j].Contains(".zip") == false)
                        continue; // If is not link, skipping line

                    // Removing the text that comes after the link
                    int zipIndex = betaHtml[j].IndexOf(".zip");
                    betaHtml[j] = betaHtml[j].Remove(zipIndex + 4);
                        
                    // Putting the link to the class variable (List)
                    files.Add(betaHtml[j]);
                }
            }
            else if (beta == false)
            {
                // Putting lines to a list
                List<string> htmlLines = new List<string>(htmlCode.Split('\n'));

                // Removing lines we don't need
                for (int i = htmlLines.Count - 1; i >= 0; --i)
                {
                    // if doesn't contain a link, that we want
                    // If the line is too short, then remove it immediately
                    if (htmlLines[i].Length < 9)
                    {
                        htmlLines.RemoveAt(i);
                        continue;
                    }
                    //Removing lines that don't contain links
                    if (!htmlLines[i].Contains("<a href=\""))
                    {
                        htmlLines.RemoveAt(i);
                    }
                }

                // Removing the front part of the filename (<a href=")
                for (int i = 0; i < htmlLines.Count; ++i)
                {
                    htmlLines[i] = htmlLines[i].Remove(0, 9);
                }
                // Removing the end part
                for (int i = 0; i < htmlLines.Count; ++i)
                {
                    htmlLines[i] = htmlLines[i].Remove(htmlLines[i].IndexOf('\"'));
                }

                // If there are no files/folders
                if (htmlLines.Count <= 0)
                {
                    MessageBox.Show("No files/folders on page", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }

                // Separating files and folders
                for (int i = htmlLines.Count - 1; i >= 0; --i)
                {
                    // if line is a folder
                    if (htmlLines[i].Contains('/'))
                    {
                        folders.Add(htmlLines[i]);
                    }
                    else
                    {
                        files.Add(htmlLines[i]);
                    }
                }
            }
            return true;
        }

        private List<string> getOnlySpecificFiles(List<string> files, string fileType)
        {
            for (int i = files.Count - 1; i >= 0; --i)
            {
                if (!files[i].Contains("." + fileType))
                {
                    files.RemoveAt(i);
                }
            }
            return files;
        }

        private void printList(List<string> printThis)
        {
            // For debugging
            string temp = "";
            for (int i = 0; i < printThis.Count; ++i)
            {
                temp += printThis[i] + "\n";
            }
            MessageBox.Show(temp);
        }
    }
}
