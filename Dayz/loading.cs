﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Net;
using System.Windows.Forms;

namespace Dayz
{
    public partial class loading : Form
    {
        public loading()
        {
            InitializeComponent();
        }
        public void setMax(int max)
        {
            progressBar.Maximum = max;
        }
        public void refreshBar()
        {
            progressBar.Refresh();
        }
        public void addOne()
        {
            if (progressBar.Value != progressBar.Maximum)
                progressBar.Value = progressBar.Value + 1;
        }
        public void setLabel(string label)
        {
            this.Text = label;
        }
        public void setValue(System.Net.DownloadProgressChangedEventArgs e)
        {
            progressBar.Value = e.ProgressPercentage;
        }
        public void setVisible(bool visible)
        {
            progressBar.Visible = visible;
        }
        public void downloadFile(string url, string file, string folder)
        {
            WebClient webClient = new WebClient();
            webClient.DownloadProgressChanged += (s, e) =>
            {
                progressBar.Value = e.ProgressPercentage;
                labelProgress.Text = e.ProgressPercentage.ToString() + " %";
            };
            webClient.DownloadFileCompleted += (s, e) =>
            {
                // Closes this form
                labelProgress.Text = "Installing beta...";
                installBeta(file, folder);
                
                this.Close();
            };
            webClient.DownloadFileAsync(new Uri(url + file), folder + file);

        }
        private void installBeta(string file, string folder)
        {
            // Removing the \ from the end of the folder, caused problems with unzip.exe
            string folder2 = folder.Remove(folder.Count() - 1);

            // Unzipping
            string command = "stuff\\unzip.exe -o " + "\"" + folder + file + "\"" + " -x *.txt -d " + "\"" + folder2 + "\"";
            fileControl fc = new fileControl();
            fc.runCommand(command);

            // Running the installer
            command = "\"" + folder + file.Remove(file.Count() - 4) + ".exe" + "\"";
            fc.runCommand(command);
            
            // Now that the installer has been copied, moving the installed exe file to it correct place
            // The folder is pointing at the beta folder, we need to copy from there, 2 folders backwards
            string gameFolder = goBackFolder(folder, 2);
            command = "copy " + "\"" + folder + "arma2oa.exe" + "\" " + "\"" + gameFolder + "\"" + " /y";
            fc.runCommand(command);
        }
        private string goBackFolder(string path, int amount)
        {
            string returnValue = path;
            for (int i = 0; i < amount; ++i)
            {
                // Removing any \ from the end
                returnValue = removeCharFromEnd(returnValue, '\\');

                // Removing the folder name that is in front
                returnValue = returnValue.Remove(returnValue.LastIndexOf('\\'));

                // Removing any possible \ that could have been left from the end
                returnValue = removeCharFromEnd(returnValue, '\\');
            }

            return returnValue;
        }
        private string removeCharFromEnd(string line, char remove)
        {
            string returnValue = line;
            // Removing from the end chars that match with the remove variable
            while (returnValue[returnValue.Length - 1] == remove)
                returnValue = returnValue.Remove(returnValue.Length - 1);

            return returnValue;
        }
    }
}
