﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Dayz
{
    public partial class Settings : Form
    {
        public Settings()
        {
            InitializeComponent();

            // Setting the settings values to what they are at the moment
            if (Properties.Settings.Default.Music == true)
                checkBoxMusic.Checked = true;
            else
                checkBoxMusic.Checked = false;

            textBoxSteam.Text = Properties.Settings.Default.SteamFolder + "\\\\Steam.exe";


            // Preventing the user from closing or resizing the window from the form buttons
            this.ControlBox = false;
        }

        private void buttonBrowse_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFile = new OpenFileDialog();
            openFile.Filter = "Steam executable (Steam.exe)|Steam.exe";

            if (openFile.ShowDialog() == DialogResult.OK)
            {
                textBoxSteam.Text = System.IO.Path.GetDirectoryName(openFile.FileName) + "\\Steam.exe";
            }
            
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            // Setting the music value
            Properties.Settings.Default.Music = checkBoxMusic.Checked;

            // Setting the new steam folder
            Properties.Settings.Default.SteamFolder = System.IO.Path.GetDirectoryName(textBoxSteam.Text);
            Properties.Settings.Default.Save();

            // Making the actual changes

            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            // No changes is to be made when cancel is pressed
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }
    }
}
