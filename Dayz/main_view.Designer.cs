﻿namespace Dayz
{
    partial class main_view
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonInstall = new System.Windows.Forms.Button();
            this.buttonUpdate = new System.Windows.Forms.Button();
            this.buttonPlay = new System.Windows.Forms.Button();
            this.labelUpdate = new System.Windows.Forms.Label();
            this.buttonSettings = new System.Windows.Forms.Button();
            this.comboBoxBeta = new System.Windows.Forms.ComboBox();
            this.buttonBetaInstall = new System.Windows.Forms.Button();
            this.buttonChangelog = new System.Windows.Forms.Button();
            this.labelBetaVersion = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // buttonInstall
            // 
            this.buttonInstall.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.buttonInstall.Location = new System.Drawing.Point(16, 32);
            this.buttonInstall.Name = "buttonInstall";
            this.buttonInstall.Size = new System.Drawing.Size(176, 26);
            this.buttonInstall.TabIndex = 0;
            this.buttonInstall.Text = "Install DayZ";
            this.buttonInstall.UseVisualStyleBackColor = true;
            this.buttonInstall.Click += new System.EventHandler(this.buttonInstall_Click);
            // 
            // buttonUpdate
            // 
            this.buttonUpdate.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.buttonUpdate.Location = new System.Drawing.Point(16, 62);
            this.buttonUpdate.Name = "buttonUpdate";
            this.buttonUpdate.Size = new System.Drawing.Size(176, 26);
            this.buttonUpdate.TabIndex = 1;
            this.buttonUpdate.Text = "Update DayZ";
            this.buttonUpdate.UseVisualStyleBackColor = true;
            this.buttonUpdate.Click += new System.EventHandler(this.buttonUpdate_Click);
            // 
            // buttonPlay
            // 
            this.buttonPlay.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.buttonPlay.Location = new System.Drawing.Point(220, 351);
            this.buttonPlay.Name = "buttonPlay";
            this.buttonPlay.Size = new System.Drawing.Size(154, 43);
            this.buttonPlay.TabIndex = 2;
            this.buttonPlay.Text = "Play Dayz";
            this.buttonPlay.UseVisualStyleBackColor = true;
            this.buttonPlay.Click += new System.EventHandler(this.buttonPlay_Click);
            // 
            // labelUpdate
            // 
            this.labelUpdate.AutoSize = true;
            this.labelUpdate.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.labelUpdate.ForeColor = System.Drawing.Color.White;
            this.labelUpdate.Location = new System.Drawing.Point(13, 91);
            this.labelUpdate.Name = "labelUpdate";
            this.labelUpdate.Size = new System.Drawing.Size(35, 13);
            this.labelUpdate.TabIndex = 3;
            this.labelUpdate.Text = "label1";
            // 
            // buttonSettings
            // 
            this.buttonSettings.Location = new System.Drawing.Point(16, 3);
            this.buttonSettings.Name = "buttonSettings";
            this.buttonSettings.Size = new System.Drawing.Size(75, 23);
            this.buttonSettings.TabIndex = 4;
            this.buttonSettings.Text = "Settings";
            this.buttonSettings.UseVisualStyleBackColor = true;
            this.buttonSettings.Click += new System.EventHandler(this.buttonSettings_Click);
            // 
            // comboBoxBeta
            // 
            this.comboBoxBeta.FormattingEnabled = true;
            this.comboBoxBeta.Location = new System.Drawing.Point(333, 33);
            this.comboBoxBeta.Name = "comboBoxBeta";
            this.comboBoxBeta.Size = new System.Drawing.Size(160, 21);
            this.comboBoxBeta.TabIndex = 5;
            // 
            // buttonBetaInstall
            // 
            this.buttonBetaInstall.Location = new System.Drawing.Point(333, 59);
            this.buttonBetaInstall.Name = "buttonBetaInstall";
            this.buttonBetaInstall.Size = new System.Drawing.Size(232, 29);
            this.buttonBetaInstall.TabIndex = 6;
            this.buttonBetaInstall.Text = "Install Beta Patch";
            this.buttonBetaInstall.UseVisualStyleBackColor = true;
            this.buttonBetaInstall.Click += new System.EventHandler(this.buttonBetaInstall_Click);
            // 
            // buttonChangelog
            // 
            this.buttonChangelog.Location = new System.Drawing.Point(499, 31);
            this.buttonChangelog.Name = "buttonChangelog";
            this.buttonChangelog.Size = new System.Drawing.Size(66, 23);
            this.buttonChangelog.TabIndex = 7;
            this.buttonChangelog.Text = "Changelog";
            this.buttonChangelog.UseVisualStyleBackColor = true;
            this.buttonChangelog.Click += new System.EventHandler(this.buttonChangelog_Click);
            // 
            // labelBetaVersion
            // 
            this.labelBetaVersion.AutoSize = true;
            this.labelBetaVersion.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.labelBetaVersion.ForeColor = System.Drawing.Color.White;
            this.labelBetaVersion.Location = new System.Drawing.Point(332, 91);
            this.labelBetaVersion.Name = "labelBetaVersion";
            this.labelBetaVersion.Size = new System.Drawing.Size(35, 13);
            this.labelBetaVersion.TabIndex = 8;
            this.labelBetaVersion.Text = "label1";
            // 
            // main_view
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Dayz.Properties.Resources.dayz_poster;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Controls.Add(this.labelBetaVersion);
            this.Controls.Add(this.buttonChangelog);
            this.Controls.Add(this.buttonBetaInstall);
            this.Controls.Add(this.comboBoxBeta);
            this.Controls.Add(this.buttonSettings);
            this.Controls.Add(this.labelUpdate);
            this.Controls.Add(this.buttonPlay);
            this.Controls.Add(this.buttonInstall);
            this.Controls.Add(this.buttonUpdate);
            this.Name = "main_view";
            this.Size = new System.Drawing.Size(584, 462);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonInstall;
        private System.Windows.Forms.Button buttonUpdate;
        private System.Windows.Forms.Button buttonPlay;
        private System.Windows.Forms.Label labelUpdate;
        private System.Windows.Forms.Button buttonSettings;
        private System.Windows.Forms.ComboBox comboBoxBeta;
        private System.Windows.Forms.Button buttonBetaInstall;
        private System.Windows.Forms.Button buttonChangelog;
        private System.Windows.Forms.Label labelBetaVersion;
    }
}
