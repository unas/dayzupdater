﻿using System;
using System.Net;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.Media;
using System.Threading;

namespace Dayz
{
    public partial class main_view : UserControl
    {
        private string dayZFolder;
        private string tempFolder;
        private string gameFolder;
        private string url;
        private webRetriever webRet;
        private webRetriever betaWeb;
        private fileControl fileCont;
        private SoundPlayer player;
        private ToolTip updateTip;


        public main_view()
        {
            InitializeComponent();

            // Creating the soundplayer
            createSoundPlayer();

            // Checking that folder is correct, and so on... (For dayz files)
            startingChecks();

            setupBeta();
        }

        private void setupBeta()
        {
            // Setting the first line to the combobox
            comboBoxBeta.Items.Add("Select beta patch to install");
            comboBoxBeta.SelectedIndex = 0;

            betaWeb = new webRetriever("http://www.arma2.com/beta-patch.php", true);

            List<string> betaFiles = new List<string>(betaWeb.getFiles());
            
            // Putting the available beta files to the comboBox
            for (int i = 0; i < betaFiles.Count(); ++i)
            {
                string temp = betaFiles[i].Substring(betaFiles[i].LastIndexOf('/') + 1, betaFiles[i].Length - betaFiles[i].LastIndexOf('/') - 5);
                comboBoxBeta.Items.Add(temp);
            }
        }

        private void startingChecks()
        {
            // Saving the folder paths to this classes private variables
            setFolderPaths();

            // Is the steam folder correct
            if (isSteamFolder())
            {

                // Making sure that the user has installed Arma II
                isArmaInstalled();

                // Creating new objects for webretirever and filecontroller
                createObjects();

                // Checking for updates, and putting info to updater
                if (isDayZInstalled() == true)
                    writeUpdateInfo(checkForUpdates());
                else
                    labelUpdate.Text = "";
            }
            else
            {
                labelUpdate.Text = "Check steam folder from settings...";
                buttonInstall.Enabled = false;
                buttonPlay.Enabled = false;
                buttonUpdate.Enabled = false;
            }

            // Updating the info for the beta patch version that is installed
            labelBetaVersion.Text = Properties.Settings.Default.BetaVersion;
        }

        private bool isSteamFolder()
        {
            if (!Directory.Exists(Properties.Settings.Default.SteamFolder))
            {
                return false;
            }
            return true;
        }

        private void writeUpdateInfo(List<string>missingFiles)
        {
            updateTip = new ToolTip();
            
            if (missingFiles.Count > 0)
            {
                string updateFiles = "";
                foreach (string i in missingFiles)
                    updateFiles += "\n" + i;

                if (missingFiles.Count == 1)
                {
                    labelUpdate.Text = "There is 1 new update";
                    updateTip.SetToolTip(labelUpdate, updateFiles);
                    //MessageBox.Show("There is 1 new update:" + updateFiles);
                }
                else if (missingFiles.Count > 1)
                {
                    labelUpdate.Text = "There are " + missingFiles.Count.ToString() + " new updates";
                    updateTip.SetToolTip(labelUpdate, updateFiles);
                    //MessageBox.Show("There are " + missingFiles.Count.ToString() + " new updates:" + updateFiles);
                }
            }
            else
            {
                labelUpdate.Text = "No new updates...";
            }
        }

        private void createObjects()
        {
            webRet = new webRetriever(url);
            fileCont = new fileControl(gameFolder);
        }

        private void setFolderPaths()
        {
            if (is64Windows() == 64) // If the user is running a 64-bit windows
            {
                //gameFolder = "C:\\Program Files (x86)\\Steam\\steamapps\\common\\arma 2 operation arrowhead\\";
                //dayZFolder = "C:\\Program Files (x86)\\Steam\\steamapps\\common\\arma 2 operation arrowhead\\@DayZ\\Addons\\";
                gameFolder = Properties.Settings.Default.SteamFolder + "\\steamapps\\common\\arma 2 operation arrowhead\\";
                dayZFolder = Properties.Settings.Default.SteamFolder + "\\steamapps\\common\\arma 2 operation arrowhead\\@DayZ\\Addons\\";

            }
            else if (is64Windows() == 32) // The user is running a 32-bit windows, what a loser
            {
                //gameFolder = "C:\\Program Files\\Steam\\steamapps\\common\\arma 2 operation arrowhead\\";
                //dayZFolder = "C:\\Program Files\\Steam\\steamapps\\common\\arma 2 operation arrowhead\\@DayZ\\Addons\\";
                gameFolder = Properties.Settings.Default.SteamFolder + "\\steamapps\\common\\arma 2 operation arrowhead\\";
                dayZFolder = Properties.Settings.Default.SteamFolder + "\\steamapps\\common\\arma 2 operation arrowhead\\@DayZ\\Addons\\";
            }
            else
                MessageBox.Show("Can't determine if you are running a 32- or a 64-bit windows!", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);

            tempFolder = dayZFolder + "installer\\";
            url = "http://cdn.armafiles.info/latest/";
        }

        private void createSoundPlayer()
        {
            if (File.Exists("stuff\\dayz.wav") && player == null)
            {
                player = new SoundPlayer("stuff/dayz.wav");
                // Don't play, if the the value is turned off from the settings
                if (Properties.Settings.Default.Music == true)
                    player.Play();
            }
        }

        private void isArmaInstalled()
        {
            if (!Directory.Exists(gameFolder))
            {
                // Game is not installed
                //MessageBox.Show("Game is not istalled.\nPlease install ARMA II and Operation arrowhead from steam first!", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                labelUpdate.Text = "Install Arma 2 and Operation arrowhead";
                buttonBetaInstall.Enabled = false;
                buttonInstall.Enabled = false;
                buttonPlay.Enabled = false;
                buttonSettings.Enabled = true;
            }
        }

        private bool isDayZInstalled()
        {
            if (!Directory.Exists(dayZFolder))
            {
                buttonInstall.Enabled = true;
                buttonUpdate.Enabled = false;
                buttonPlay.Enabled = false;

                return false;
            }
            else
            {
                buttonInstall.Enabled = false;
                buttonUpdate.Enabled = true;
                buttonPlay.Enabled = true;

                return true;
            }
        }

        private int is64Windows()
        {
            if (Directory.Exists("C:\\Program Files (x86)"))
                return 64;
            else
                return 32;
        }

        private void buttonInstall_Click(object sender, EventArgs e)
        {
            // Creating the dayz folder, also the temp folder where we will download that files from the server
            Directory.CreateDirectory(tempFolder);

            //webRet = new webRetriever(url);
            //fileCont = new fileControl(gameFolder);

            List<string> serverFiles = new List<string>();
            // Getting the list of files that are on the server
            serverFiles = fileCont.checkForNewFiles(webRet.getFiles());

            if (serverFiles.Count > 0)
            {
                // Getting the files that are missing
                //webRet.downloadFiles(serverFiles, tempFolder);
                Thread thread = new Thread(() => webRet.downloadFiles(serverFiles, tempFolder));
                thread.Start();

                while(true)
                {
                    Thread.Sleep(1000);
                    if (!thread.IsAlive)
                        break;
                }

                // Installing missing files
                fileCont.installFiles(serverFiles, tempFolder, dayZFolder);

                if (serverFiles.Count == 1)
                    MessageBox.Show("Installed " + serverFiles.Count + " new file");
                else
                    MessageBox.Show("Installed " + serverFiles.Count + " new files");

                buttonInstall.Enabled = false;
                buttonUpdate.Enabled = true;
                buttonPlay.Enabled = true;
            }
            else
            {
                MessageBox.Show("No files on server!");
            }

            // Writes update info. Yes, I know. There is no need to check for updates, since we just installed the newest files.
            // But this method was ready, and I didn't want to write anything new
            writeUpdateInfo(checkForUpdates());
        }

        private List<string> checkForUpdates()
        {
            // Getting the list of files that are missing, or if there are newer
            List<string> missingFiles = new List<string>(fileCont.checkForNewFiles(webRet.getFiles()));

            return missingFiles;
        }

        private void buttonUpdate_Click(object sender, EventArgs e)
        {
            if (Directory.Exists(dayZFolder))
            {
                // Create the dayz folder, if it doesn't exist
                if (!fileCont.checkTempFolderExists())
                    fileCont.createTempFolder();

                List<string>missingFiles = new List<string>();
                // Getting the list of files that are missing, or there are newer
                missingFiles = fileCont.checkForNewFiles(webRet.getFiles());

                if (missingFiles.Count > 0)
                {
                    // Getting the files that are missing
                    webRet.downloadFiles(missingFiles, tempFolder);

                    // Installing missing files
                    fileCont.installFiles(missingFiles, tempFolder, dayZFolder);

                    if (missingFiles.Count == 1)
                        MessageBox.Show("Installed " + missingFiles.Count + " new file");
                    else
                        MessageBox.Show("Installed " + missingFiles.Count + " new files");

                    labelUpdate.Text = "No new updates...";
                    updateTip = null;
                }
                else
                {
                    MessageBox.Show("No updates available!");
                    labelUpdate.Text = "No new updates...";
                    updateTip = null;
                }
            }
            else
                MessageBox.Show("DayZ installation folder not found", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void buttonPlay_Click(object sender, EventArgs e)
        {
            player.Stop();
            fileCont = new fileControl(gameFolder);
            fileCont.playDayZ();
        }

        private void buttonSettings_Click(object sender, EventArgs e)
        {
            Settings settings = new Settings();
            if (settings.ShowDialog() == DialogResult.OK)
            {
                // If the music is suppose to play
                if (Properties.Settings.Default.Music)
                {
                    // Just making sure that we have created the player
                    createSoundPlayer(); // Creates sound player if it doesn't exist
                        
                    player.Play();
                }
                else
                    player.Stop();

                // Making the same checks for updates as earlier
                startingChecks();
            }
        }

        private void buttonBetaInstall_Click(object sender, EventArgs e)
        {
            // Making sure that that file that we want to download isn't the text "Select beta patch to install"
            if (comboBoxBeta.SelectedIndex != 0)
            {
                string fileToInstall = comboBoxBeta.Text + ".zip";
                webRetriever downloader = new webRetriever();

                downloader.url = "ftp://downloads.bistudio.com/arma2.com/update/beta/";
                downloader.downloadFiles(fileToInstall, gameFolder + "Expansion\\beta\\");

                // updating the version number of the installed beta patch
                string versionNumber = fileToInstall.Substring(0, fileToInstall.Length - 4); // Removing ".zip" from the end
                versionNumber = versionNumber.Split('_')[versionNumber.Split('_').Length - 1];

                Properties.Settings.Default.BetaVersion = "Installed version: " + versionNumber;
                Properties.Settings.Default.Save();

                // Changing the label text on the front page with the new beta patch version
                labelBetaVersion.Text = Properties.Settings.Default.BetaVersion;
            }
        }

        private void buttonChangelog_Click(object sender, EventArgs e)
        {
            // Making sure that that file that we want to show the changelog for the text "Select beta patch to install"
            if (comboBoxBeta.SelectedIndex != 0)
            {
                string fileToDownload = comboBoxBeta.Text + ".log";

                webRetriever downloader = new webRetriever();
                string changelog = downloader.downloadText("ftp://downloads.bistudio.com/arma2.com/update/beta/" + fileToDownload);

                // Printing if the received text isn't empty
                if (changelog != "")
                {
                    LogView log = new LogView(comboBoxBeta.Text);
                    log.writeText(changelog);
                    log.Show();
                }
            }
        }
    }
}
