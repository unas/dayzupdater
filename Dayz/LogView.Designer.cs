﻿namespace Dayz
{
    partial class LogView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxChangelog = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // textBoxChangelog
            // 
            this.textBoxChangelog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxChangelog.Location = new System.Drawing.Point(0, 0);
            this.textBoxChangelog.Multiline = true;
            this.textBoxChangelog.Name = "textBoxChangelog";
            this.textBoxChangelog.ReadOnly = true;
            this.textBoxChangelog.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBoxChangelog.Size = new System.Drawing.Size(515, 530);
            this.textBoxChangelog.TabIndex = 0;
            // 
            // LogView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(515, 530);
            this.Controls.Add(this.textBoxChangelog);
            this.Name = "LogView";
            this.ShowIcon = false;
            this.Text = "LogView";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxChangelog;
    }
}