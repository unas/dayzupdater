﻿namespace Dayz
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindow));
            this.labelVersion = new System.Windows.Forms.Label();
            this.labelCopyright = new System.Windows.Forms.Label();
            this.main_view2 = new Dayz.main_view();
            this.main_view1 = new Dayz.main_view();
            this.SuspendLayout();
            // 
            // labelVersion
            // 
            this.labelVersion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.labelVersion.AutoSize = true;
            this.labelVersion.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.labelVersion.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.labelVersion.Location = new System.Drawing.Point(503, 440);
            this.labelVersion.Name = "labelVersion";
            this.labelVersion.Size = new System.Drawing.Size(69, 13);
            this.labelVersion.TabIndex = 1;
            this.labelVersion.Text = "Version 0.3.3";
            // 
            // labelCopyright
            // 
            this.labelCopyright.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.labelCopyright.AutoSize = true;
            this.labelCopyright.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.labelCopyright.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.labelCopyright.Location = new System.Drawing.Point(12, 440);
            this.labelCopyright.Name = "labelCopyright";
            this.labelCopyright.Size = new System.Drawing.Size(69, 13);
            this.labelCopyright.TabIndex = 2;
            this.labelCopyright.Text = "unas © 2012";
            // 
            // main_view2
            // 
            this.main_view2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("main_view2.BackgroundImage")));
            this.main_view2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.main_view2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.main_view2.Location = new System.Drawing.Point(0, 0);
            this.main_view2.Name = "main_view2";
            this.main_view2.Size = new System.Drawing.Size(584, 462);
            this.main_view2.TabIndex = 0;
            // 
            // main_view1
            // 
            this.main_view1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("main_view1.BackgroundImage")));
            this.main_view1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.main_view1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.main_view1.Location = new System.Drawing.Point(0, 0);
            this.main_view1.Name = "main_view1";
            this.main_view1.Size = new System.Drawing.Size(580, 443);
            this.main_view1.TabIndex = 0;
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 462);
            this.Controls.Add(this.labelCopyright);
            this.Controls.Add(this.labelVersion);
            this.Controls.Add(this.main_view2);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(600, 500);
            this.MinimumSize = new System.Drawing.Size(600, 500);
            this.Name = "MainWindow";
            this.Text = "DayZ";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private main_view main_view1;
        private main_view main_view2;
        private System.Windows.Forms.Label labelVersion;
        private System.Windows.Forms.Label labelCopyright;

    }
}

