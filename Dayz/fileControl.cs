﻿using System;
using System.Threading;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;
using System.Diagnostics;

namespace Dayz
{
    class fileControl
    {
        private string dayZDir;
        private string tempFolder;
        private string gameFolder;

        public string filetype(string file)
        {
            List<string> fileParts = new List<string>(file.Split('.'));
            return fileParts[fileParts.Count - 1]; // Returning the last part of the index
        }

        public fileControl(string folder)
        {
            gameFolder = folder;
            dayZDir = gameFolder + "@DayZ\\Addons\\";
            tempFolder = dayZDir + "installer\\";
        }

        public fileControl()
        {

        }

        // returns true if the DayZ folder exists
        public bool checkTempFolderExists()
        {
            if (!Directory.Exists(tempFolder))
                return false;
            else
                return true;
        }

        public void createTempFolder()
        {
            Directory.CreateDirectory(Path.GetDirectoryName(tempFolder));
        }

        // Will extract files to gameFolder
        public void installFiles(List<string>files2Install, string fromFolder, string toFolder)
        {
            // Progress bar
            loading progress = new loading();
            progress.setLabel("Installing...");
            progress.setMax(files2Install.Count);
            progress.Show();
            progress.addOne();
            progress.refreshBar();

            for (int i = 0; i < files2Install.Count; ++i)
            {
                // Only rar files are unrarred, the rest is copied directly to the game folder
                if (filetype(files2Install[i]) == "rar")
                {
                    string command = "stuff\\Rar.exe x " + "\"" + fromFolder + files2Install[i] + "\"" + " " + "\"" + toFolder + "\"" + " -Y"; // The -Y forces overwrite
                    runCommand(command);

                    progress.addOne();
                    progress.refreshBar();
                }
                else // Is something else than rar 
                {
                    string command = "copy " + "\"" + fromFolder + files2Install[i] + "\" " + "\"" + toFolder + "\"" + " /y"; // /y forces overwrite
                    runCommand(command);

                    progress.addOne();
                    progress.refreshBar();
                }

                
            }
            progress.Close();
        }

        //Return the files that are missing or if newer can be found on the site
        public List<string> checkForNewFiles(List<string>httpFiles)
        {
            // Checking if "installer" folder exists, if not, creating it
            if (!Directory.Exists(tempFolder))
                Directory.CreateDirectory(Path.GetDirectoryName(tempFolder));

            string[] filePaths = Directory.GetFiles(tempFolder);
            List<string> files = new List<string>();
            foreach (string i in filePaths)
            {
                // We now have files in their full address, we need to remove everything except the filename
                List<string> temp = new List<string>(i.Split('\\'));
                files.Add(temp[temp.Count - 1]);
            }

            // Now we have the files on the server, and the files on the computer, we will compare those and return what need to be downloaded
            // Going through all the files
            List<string> files2Update = new List<string>();
            for (int i = 0; i < httpFiles.Count; ++i)
            {
                if (searchList(files, httpFiles[i]) == -1)
                {
                    // If a file from the server doesn't exist on this computer
                    files2Update.Add(httpFiles[i]);
                }
            }


            return files2Update;
        }


        // Runs the given command in the command prompt
        public void runCommand(string command_)
        {
            Process process = new Process();
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.WindowStyle = ProcessWindowStyle.Hidden;
            startInfo.FileName = @"cmd.exe";
            startInfo.Arguments = "/C " + command_;
            process.StartInfo = startInfo;
            process.Start();

            process.WaitForExit();
        }

        public string getGameDirectory()
        {
            return dayZDir;
        }
        private int searchList(List<string>list, string search, bool exactSearch = true)
        {
            for (int i = 0; i < list.Count; ++i)
            {
                if (exactSearch == true && list[i] == search)
                    return i;
                else if (exactSearch == false && list[i].Contains(search))
                    return i;
            }
            return -1;
        }

        public void playDayZ()
        {
            string command = "stuff\\run.cmd";
            runCommand(command);
        }
    }
}
