﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Dayz
{
    public partial class LogView : Form
    {
        public LogView(string filename)
        {
            InitializeComponent();

            this.Text = "Changelog: " + filename;
        }
        public void writeText(string text)
        {
            List<string> textLines = new List<string>(text.Split('\n'));
            outputLog(textLines);
        }
        public void writeText(List<string>textLines)
        {
            outputLog(textLines);
        }
        private void outputLog(List<string>textLines)
        {
            // Clearing the textbox
            textBoxChangelog.Text = "";

            for(int i = 0; i < textLines.Count(); ++i)
            {
                textBoxChangelog.Text += textLines[i] + '\n';
            }
        }
    }
}
